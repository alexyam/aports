# Contributor: Stuart Cardall <developer@it-offshore.co.uk>
# Maintainer: Stuart Cardall <developer@it-offshore.co.uk>
pkgname=py3-tox
_pkgname=${pkgname#py3-*}
pkgver=3.21.2
pkgrel=0
pkgdesc="virtualenv management and test command line tool"
options="!check" #  Requires community/py3-pathlib2, and unpackaged flaky
url="https://tox.readthedocs.org/"
arch="noarch"
license="MIT"
depends="
	py3-packaging
	py3-pluggy
	py3-py
	py3-six
	py3-virtualenv
	py3-toml
	py3-filelock
	py3-setuptools
	"
checkdepends="py3-pytest"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-tox" # Backwards compatibility
provides="py-tox=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

check() {
	python3 setup.py test
}

sha512sums="d2dac084d5da1f46efa909af9583cb72881afb92c1e3c513bd80e9805e3a86c92733207163ced6928826cb294125997545ccd014fd1a22a928191bc5aff1e284  tox-3.21.2.tar.gz"
