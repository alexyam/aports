# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=cmake
pkgver=3.19.3
pkgrel=0
pkgdesc="Cross-platform, open-source make system"
url="https://www.cmake.org/"
arch="all"
license="BSD-3-Clause"
makedepends="bzip2-dev curl-dev expat-dev libarchive-dev linux-headers
	libuv-dev ncurses-dev rhash-dev xz-dev zlib-dev py3-sphinx"
options="!check"
checkdepends="file musl-utils"
subpackages="$pkgname-doc $pkgname-bash-completion
	"
case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac

source="https://www.cmake.org/files/$_v/cmake-$pkgver.tar.gz
	"

_parallel_opt() {
	local i n
	for i in $MAKEOPTS; do
		case "$i" in
			-j*) n=${i#-j};;
		esac;
	done
	[ -n "$n" ] && echo "--parallel $n"
}

build() {
	# jsoncpp needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version of jsoncpp.
	# Do NOT remove --no-system-jsoncpp unless you consulted
	# maintainer
	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--sphinx-man \
		--system-libs \
		--no-system-jsoncpp \
		$(_parallel_opt)
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE bin/ctest
}

package() {
	cd $startdir/src/$pkgname-$pkgver
	make DESTDIR="$pkgdir" install
}

sha512sums="54c91fa76882152d3933b696d7e547bc4edf6f97f3810e8dd57ca28c98e9fcaf757eac7c77e1e7fab88aced10937ed354917aeac3997edef406313dcf76ba0c5  cmake-3.19.3.tar.gz"
